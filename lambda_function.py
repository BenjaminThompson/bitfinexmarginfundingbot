from __future__ import print_function
import logging
import json
import BFX


FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def get_btc_deposit_balance(balances):
    return [float(b['available']) for b in balances if b['type'] == 'deposit' and b['currency'] == 'btc'][0]


def btc_in_usd(balance):
    return float(BFX.ticker('BTCUSD')['last_price']) * balance


def calculate_middle_rate():
    book = BFX.lendbook('BTC')
    bid = float(book['bids'][0]['rate'])
    ask = float(book['asks'][0]['rate'])
    return bid+((ask-bid)/2)


def lambda_handler(event, context):
    logger.log(logging.INFO, "Starting...")
    offers = BFX.offers()
    for offer in offers:
        logger.info("Cancelling offer {}".format(json.dumps(offer)))
        BFX.cancel_offer(offer['id'])

    balance = get_btc_deposit_balance(BFX.balances())
    if btc_in_usd(balance) > 50.0:
        logger.info("Available funds to invest: {}".format(str(balance)))
        rate = calculate_middle_rate()
        logger.info("Offering at rate: {}".format("%.3f" % rate))
        offer_response = BFX.offer_new(amount=str(balance), rate="%.3f" % rate)
        logger.info("Offer Meta: {}".format(json.dumps(offer_response)))
    else:
        logger.info("Available funds ({}) amount to less than 50 USD.".format(str(balance)))


if __name__ == "__main__":
    lambda_handler('', '')
