from __future__ import print_function
from credentials import *
import requests
import json
from time import time
import base64
from Crypto.Hash import SHA384, HMAC


def __init__():
    pass


def nonce():
    return str(int(time()*1000000))


def authorized_request(payload):
    payload_s = base64.b64encode(json.dumps(payload))
    hmac = HMAC.new(key=API_KEY_SECRET, digestmod=SHA384)
    hmac.update(payload_s)
    signature = hmac.hexdigest()
    headers = {
        'X-BFX-APIKEY': API_KEY,
        'X-BFX-PAYLOAD': payload_s,
        'X-BFX-SIGNATURE': signature
    }
    full_url = "https://api.bitfinex.com{0}".format(payload['request'])
    result = requests.post(url=full_url, data=payload, headers=headers)
    return result


def balances():
    payload = {
        'request': '/v1/balances',
        'nonce': nonce()
    }
    return json.loads(authorized_request(payload).content)


def offers():
    payload = {
        'request': '/v1/offers',
        'nonce': nonce()
    }
    return json.loads(authorized_request(payload).content)


def credits():
    payload = {
        'request': '/v1/credits',
        'nonce': nonce()
    }
    return json.loads(authorized_request(payload).content)


def offer_status(offer_id):
    payload = {
        'request': '/v1/offer/status',
        'nonce': nonce(),
        'offer_id': offer_id
    }
    return json.loads(authorized_request(payload).content)


def cancel_offer(offer_id):
    payload = {
        'request': '/v1/offer/cancel',
        'nonce': nonce(),
        'offer_id': offer_id
    }
    return json.loads(authorized_request(payload).content)


def offer_new(currency='BTC', amount='0.0',
              rate='10.0', period=30, direction='lend'):
    payload = {
        'request': '/v1/offer/new',
        'nonce': nonce(),
        'currency': currency,
        'amount': amount,
        'rate': rate,
        'period': period,
        'direction': direction
    }
    return json.loads(authorized_request(payload).content)


def lendbook(currency):
    return json.loads(
        requests.get("https://api.bitfinex.com/v1/lendbook/{}?limit_bids=1&limit_asks=1".format(currency)).content
    )


def ticker(symbol):
    return json.loads(
        requests.get("https://api.bitfinex.com/v1/pubticker/{}".format(symbol)).content
    )
